<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>pic_user_bot_</name>
   <tag></tag>
   <elementGuidId>603f1d6a-e87e-4f80-ba23-3332f7f57353</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.bot_column</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='root']/div/div[2]/div/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>99ba1d26-eaab-4fad-a60f-e93ac02efef5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>bot_column</value>
      <webElementGuid>18aa4dab-ab58-4f63-8e35-a7b8a5e26af2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;root&quot;)/div[1]/div[@class=&quot;login_wrapper&quot;]/div[@class=&quot;login_wrapper-inner&quot;]/div[@class=&quot;bot_column&quot;]</value>
      <webElementGuid>1ebf41ac-2fa7-4131-8357-ee1219a93390</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='root']/div/div[2]/div/div[2]</value>
      <webElementGuid>beb7da06-7047-46d8-8b84-b488d57b659d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div[2]</value>
      <webElementGuid>2b9f2868-1fc1-424f-a6c4-53c88a7c8b37</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
