<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Remove</name>
   <tag></tag>
   <elementGuidId>afb9d2e7-9ea3-4e72-968b-dd542007ade3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#remove-sauce-labs-backpack</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='remove-sauce-labs-backpack']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>fb3774d0-8372-4d8a-97fd-9076eb667e57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn_secondary btn_small cart_button</value>
      <webElementGuid>eb873418-ee49-4b4e-9172-43bc258c7f07</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-test</name>
      <type>Main</type>
      <value>remove-sauce-labs-backpack</value>
      <webElementGuid>2fbf858b-1e47-404d-93d7-7c68c429f2b5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>remove-sauce-labs-backpack</value>
      <webElementGuid>e3f864df-e5d1-4298-b428-1a5c4a632ba2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>remove-sauce-labs-backpack</value>
      <webElementGuid>f4a650f8-2d71-4bab-ba85-3a65692090a2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Remove</value>
      <webElementGuid>89edf85c-f400-4abb-9539-6a518c89d13b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;remove-sauce-labs-backpack&quot;)</value>
      <webElementGuid>a1c46b5b-f5e3-493d-bf3f-cbe60d843344</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='remove-sauce-labs-backpack']</value>
      <webElementGuid>091bdca9-21af-4996-8943-06e20f13a924</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='cart_contents_container']/div/div/div[3]/div[2]/div[2]/button</value>
      <webElementGuid>8b7b27eb-af1f-4640-8baa-4b1bf054af4f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='$29.99'])[1]/following::button[1]</value>
      <webElementGuid>bc85fd3a-d824-4c16-aa72-f822f8150cd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sauce Labs Backpack'])[1]/following::button[1]</value>
      <webElementGuid>aad8cdcc-4118-4384-877f-d59b50e00536</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Continue Shopping'])[1]/preceding::button[1]</value>
      <webElementGuid>392e559b-d933-4f6f-ab8c-8a81af3f2157</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Checkout'])[1]/preceding::button[2]</value>
      <webElementGuid>5b424290-03a0-4c26-9396-d4f951da0fe6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Remove']/parent::*</value>
      <webElementGuid>481ab14b-c7c9-45e2-b0a3-aa77c0e36eb4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/button</value>
      <webElementGuid>73f1e73a-442d-453c-b393-85473027947e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@id = 'remove-sauce-labs-backpack' and @name = 'remove-sauce-labs-backpack' and (text() = 'Remove' or . = 'Remove')]</value>
      <webElementGuid>157b2c40-2483-497e-b458-3f91cf1b0f1a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
