<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>sosmed_LinkedIn</name>
   <tag></tag>
   <elementGuidId>4f918c23-67b7-48b3-9548-41fe76c361bb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>li.social_linkedin > a</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='page_wrapper']/footer/ul/li[3]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>0cc9b6f5-a79e-46d9-bb48-319014169c11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://www.linkedin.com/company/sauce-labs/</value>
      <webElementGuid>6f3f4935-4877-4537-860a-4c8136d8b54f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>target</name>
      <type>Main</type>
      <value>_blank</value>
      <webElementGuid>492b55f5-da8d-4ca9-bbf7-8304c64e4003</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rel</name>
      <type>Main</type>
      <value>noreferrer</value>
      <webElementGuid>572a440e-f4f1-4811-a2fc-70881838e1dd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>LinkedIn</value>
      <webElementGuid>6e3c9c91-93c3-4257-a08a-5eb6d26c7269</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;page_wrapper&quot;)/footer[@class=&quot;footer&quot;]/ul[@class=&quot;social&quot;]/li[@class=&quot;social_linkedin&quot;]/a[1]</value>
      <webElementGuid>0dad6d2c-d685-4a2b-b199-a8f8901bb902</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='page_wrapper']/footer/ul/li[3]/a</value>
      <webElementGuid>7fb4928f-fad1-49ec-8983-c9748b7adb1f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'LinkedIn')]</value>
      <webElementGuid>f260d99b-00bb-4f97-b032-461fc6e1b2bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Facebook'])[1]/following::a[1]</value>
      <webElementGuid>f33509b5-9ae0-452f-baee-7646fe6c8cfa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Twitter'])[1]/following::a[2]</value>
      <webElementGuid>0380a99d-3f97-4b1c-b48e-ceed12fad86b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::a[1]</value>
      <webElementGuid>ad2919e4-1812-4176-97c2-de9d815d965d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='LinkedIn']/parent::*</value>
      <webElementGuid>c55f3fdd-8ad5-42b8-b433-d1114d932054</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://www.linkedin.com/company/sauce-labs/')]</value>
      <webElementGuid>1f108a45-39d0-4bbb-a824-3fc3239b638a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[3]/a</value>
      <webElementGuid>93cebcbd-78ae-4b62-800e-d38b724d2671</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://www.linkedin.com/company/sauce-labs/' and (text() = 'LinkedIn' or . = 'LinkedIn')]</value>
      <webElementGuid>ebd9ede8-849a-417d-9cf2-d4ef859e7a00</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
