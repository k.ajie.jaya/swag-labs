<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_bike light</name>
   <tag></tag>
   <elementGuidId>7f57f1ea-b789-47bb-b7d9-967b93a166eb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#item_0_img_link > img.inventory_item_img</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='item_0_img_link']/img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
      <webElementGuid>2a7fca65-64be-4d84-a34c-5cf28da3c0de</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Sauce Labs Bike Light</value>
      <webElementGuid>d67587d8-3e8a-4651-9268-1bbdc4840145</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>inventory_item_img</value>
      <webElementGuid>34778d9c-27d3-4e21-8ca9-23f054f70f8e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>/static/media/bike-light-1200x1500.a0c9caae.jpg</value>
      <webElementGuid>8690f3d8-8046-4e1b-8437-db42b988c99d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;item_0_img_link&quot;)/img[@class=&quot;inventory_item_img&quot;]</value>
      <webElementGuid>e0c678c9-ab7b-489d-b060-fad1fcffcbed</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//a[@id='item_0_img_link']/img</value>
      <webElementGuid>21175310-9a11-4f67-856f-2bb018c5808d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>//img[@alt='Sauce Labs Bike Light']</value>
      <webElementGuid>ac5b0eb0-1412-4ebb-9c57-b2bef2f89d48</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/a/img</value>
      <webElementGuid>77c5726f-bb42-4357-8ff7-ffc7cc2babb1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//img[@alt = 'Sauce Labs Bike Light' and @src = '/static/media/bike-light-1200x1500.a0c9caae.jpg']</value>
      <webElementGuid>fb7b643d-3096-4e20-98a1-07541d4d2d20</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
